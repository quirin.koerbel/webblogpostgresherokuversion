package de.awacademy.test.webblog;

import de.awacademy.test.webblog.category.Category;
import de.awacademy.test.webblog.category.CategoryRepository;
import de.awacademy.test.webblog.comment.Comment;
import de.awacademy.test.webblog.comment.CommentRepository;
import de.awacademy.test.webblog.entry.Entry;
import de.awacademy.test.webblog.entry.EntryRepository;
import de.awacademy.test.webblog.entryVersion.EntryVersion;
import de.awacademy.test.webblog.entryVersion.EntryVersionRepository;
import de.awacademy.test.webblog.images.Image;
import de.awacademy.test.webblog.images.ImageRepository;
import de.awacademy.test.webblog.user.User;
import de.awacademy.test.webblog.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootApplication
public class WebblogApplication {

    @Autowired
    UserRepository userRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    EntryRepository entryRepository;
    @Autowired
    ImageRepository imageRepository;
    @Autowired
    EntryVersionRepository entryVersionRepository;
    @Autowired
    CommentRepository commentRepository;



    public static void main(String[] args) {
        SpringApplication.run(WebblogApplication.class, args);
    }

    @PostConstruct
   @Transactional
    public void dataFiller() throws IOException {
    if(true){

//        makeUsers();

   //    makeCategorys();


//        addPics();

    //    addEntrys();


    }




    }

    private void addEntrys() {

        Entry entryEntity = new Entry(userRepository.findFirstById(1));
        entryEntity.setImage(imageRepository.findFirstById(1L));

        entryRepository.save(entryEntity);


        entryEntity.getCategories().clear();
        entryEntity.getCategories().add(categoryRepository.getFirstById(4L));

        entryRepository.save(entryEntity);


        EntryVersion entryVersion = new EntryVersion("Wer freut sich auf JavaScript?", "JavaScript: Die Community-Konferenz RuhrJS freut sich auf Einreichungen\n" +
                "Noch bis Ende April haben JavaScript-Entwickler die Möglichkeit, am Call for Papers der Community-Konferenz RuhrJS teilzunehmen. Gesucht sind englischsprachige Vorträge von 30 Minuten Länge. \n Der Call for Papers der Community-Konferenz RuhrJS ist noch bis Ende April geöffnet. JavaScript-Entwickler aus aller Welt sind dazu eingeladen, Vorschläge für einen englischsprachigen Vortrag von 30 Minuten Länge einzureichen. Die Konferenz findet am 13. und 14. Oktober in Bochum statt.\n" +
                "\n" +
                "Die RuhrJS ist eine Konferenz für die JavaScript-Community. Sie fand erstmalig im Jahr 2016 statt und kann jährlich eine Teilnehmerzahl von circa 200 Personen vorweisen. Besonders das Thema Diversität nimmt bei der Konferenz einen hohen Stellwert ein. So können Teilnehmer beim Ticketkauf unterrepräsentierte Gruppen in der IT mit einem individuellen Prozentsatz an zusätzlichen Gebühren unterstützen. Laut den Veranstaltern konnten in den vergangenen Jahren circa 20 Prozent der verkauften Tickets an diese Gruppen übergeben werden.\n" +
                "\n" +
                "Die Idee der RuhrJS entstand im Rahmen der PottJS Usergroup. Federführende Kraft hinter der Konferenz ist Madeleine Neumann, die heise Developer im Rahmen der Interviewserie \"Die jungen Wilden\" bereits interviewt hat. Wer über den Verlauf der Veranstaltung auf dem Laufenden gehalten werden will, kann der RuhrJS auf Twitter folgen. (bbo) \n", entryEntity, userRepository.findFirstById(1));
        entryVersionRepository.save(entryVersion);

   //------------------------------------------------------------------------------------------------------

         entryEntity = new Entry(userRepository.findFirstById(2));
        entryEntity.setImage(imageRepository.findFirstById(1L));

        entryRepository.save(entryEntity);


        entryEntity.getCategories().clear();
        entryEntity.getCategories().add(categoryRepository.getFirstById(1L));

        entryRepository.save(entryEntity);


         entryVersion = new EntryVersion("Java Enum", "Enum Types\n" +
                 "An enum type is a special data type that enables for a variable to be a set of predefined constants. The variable must be equal to one of the values that have been predefined for it. Common examples include compass directions (values of NORTH, SOUTH, EAST, and WEST) and the days of the week.\n" +
                 "\n" +
                 "Because they are constants, the names of an enum type's fields are in uppercase letters.\n" +
                 "\n" +
                 "In the Java programming language, you define an enum type by using the enum keyword. For example, you would specify a days-of-the-week enum type as:\n" +
                 "\n" +
                 "\n" +
                 "public enum Day {\n" +
                 "    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,\n" +
                 "    THURSDAY, FRIDAY, SATURDAY \n" +
                 "}\n" +
                 "You should use enum types any time you need to represent a fixed set of constants. That includes natural enum types such as the planets in our solar system and data sets where you know all possible values at compile time—for example, the choices on a menu, command line flags, and so on.\n" +
                 "\n" +
                 "Here is some code that shows you how to use the Day enum defined above:\n" +
                 "\n" +
                 "\n" +
                 "public class EnumTest {\n" +
                 "    Day day;\n" +
                 "    \n" +
                 "    public EnumTest(Day day) {\n" +
                 "        this.day = day;\n" +
                 "    }\n" +
                 "    \n" +
                 "    public void tellItLikeItIs() {\n" +
                 "        switch (day) {\n" +
                 "            case MONDAY:\n" +
                 "                System.out.println(\"Mondays are bad.\");\n" +
                 "                break;\n" +
                 "                    \n" +
                 "            case FRIDAY:\n" +
                 "                System.out.println(\"Fridays are better.\");\n" +
                 "                break;\n" +
                 "                         \n" +
                 "            case SATURDAY: case SUNDAY:\n" +
                 "                System.out.println(\"Weekends are best.\");\n" +
                 "                break;\n" +
                 "                        \n" +
                 "            default:\n" +
                 "                System.out.println(\"Midweek days are so-so.\");\n" +
                 "                break;\n" +
                 "        }\n" +
                 "    }\n" +
                 "    \n" +
                 "    public static void main(String[] args) {\n" +
                 "        EnumTest firstDay = new EnumTest(Day.MONDAY);\n" +
                 "        firstDay.tellItLikeItIs();\n" +
                 "        EnumTest thirdDay = new EnumTest(Day.WEDNESDAY);\n" +
                 "        thirdDay.tellItLikeItIs();\n" +
                 "        EnumTest fifthDay = new EnumTest(Day.FRIDAY);\n" +
                 "        fifthDay.tellItLikeItIs();\n" +
                 "        EnumTest sixthDay = new EnumTest(Day.SATURDAY);\n" +
                 "        sixthDay.tellItLikeItIs();\n" +
                 "        EnumTest seventhDay = new EnumTest(Day.SUNDAY);\n" +
                 "        seventhDay.tellItLikeItIs();\n" +
                 "    }\n" +
                 "}\n" +
                 "The output is:\n" +
                 "\n" +
                 "Mondays are bad.\n" +
                 "Midweek days are so-so.\n" +
                 "Fridays are better.\n" +
                 "Weekends are best.\n" +
                 "Weekends are best.\n" +
                 "Java programming language enum types are much more powerful than their counterparts in other languages. The enum declaration defines a class (called an enum type). The enum class body can include methods and other fields. The compiler automatically adds some special methods when it creates an enum. For example, they have a static values method that returns an array containing all of the values of the enum in the order they are declared. This method is commonly used in combination with the for-each construct to iterate over the values of an enum type. For example, this code from the Planet class example below iterates over all the planets in the solar system.\n" +
                 "\n" +
                 "for (Planet p : Planet.values()) {\n" +
                 "    System.out.printf(\"Your weight on %s is %f%n\",\n" +
                 "                      p, p.surfaceWeight(mass));\n" +
                 "}\n" +
                 "Note: All enums implicitly extend java.lang.Enum. Because a class can only extend one parent (see Declaring Classes), the Java language does not support multiple inheritance of state (see Multiple Inheritance of State, Implementation, and Type), and therefore an enum cannot extend anything else.\n" +
                 "In the following example, Planet is an enum type that represents the planets in the solar system. They are defined with constant mass and radius properties.\n" +
                 "\n" +
                 "Each enum constant is declared with values for the mass and radius parameters. These values are passed to the constructor when the constant is created. Java requires that the constants be defined first, prior to any fields or methods. Also, when there are fields and methods, the list of enum constants must end with a semicolon.\n" +
                 "\n" +
                 "Note: The constructor for an enum type must be package-private or private access. It automatically creates the constants that are defined at the beginning of the enum body. You cannot invoke an enum constructor yourself.\n" +
                 "In addition to its properties and constructor, Planet has methods that allow you to retrieve the surface gravity and weight of an object on each planet. Here is a sample program that takes your weight on earth (in any unit) and calculates and prints your weight on all of the planets (in the same unit):\n" +
                 "\n" +
                 "\n" +
                 "public enum Planet {\n" +
                 "    MERCURY (3.303e+23, 2.4397e6),\n" +
                 "    VENUS   (4.869e+24, 6.0518e6),\n" +
                 "    EARTH   (5.976e+24, 6.37814e6),\n" +
                 "    MARS    (6.421e+23, 3.3972e6),\n" +
                 "    JUPITER (1.9e+27,   7.1492e7),\n" +
                 "    SATURN  (5.688e+26, 6.0268e7),\n" +
                 "    URANUS  (8.686e+25, 2.5559e7),\n" +
                 "    NEPTUNE (1.024e+26, 2.4746e7);\n" +
                 "\n" +
                 "    private final double mass;   // in kilograms\n" +
                 "    private final double radius; // in meters\n" +
                 "    Planet(double mass, double radius) {\n" +
                 "        this.mass = mass;\n" +
                 "        this.radius = radius;\n" +
                 "    }\n" +
                 "    private double mass() { return mass; }\n" +
                 "    private double radius() { return radius; }\n" +
                 "\n" +
                 "    // universal gravitational constant  (m3 kg-1 s-2)\n" +
                 "    public static final double G = 6.67300E-11;\n" +
                 "\n" +
                 "    double surfaceGravity() {\n" +
                 "        return G * mass / (radius * radius);\n" +
                 "    }\n" +
                 "    double surfaceWeight(double otherMass) {\n" +
                 "        return otherMass * surfaceGravity();\n" +
                 "    }\n" +
                 "    public static void main(String[] args) {\n" +
                 "        if (args.length != 1) {\n" +
                 "            System.err.println(\"Usage: java Planet <earth_weight>\");\n" +
                 "            System.exit(-1);\n" +
                 "        }\n" +
                 "        double earthWeight = Double.parseDouble(args[0]);\n" +
                 "        double mass = earthWeight/EARTH.surfaceGravity();\n" +
                 "        for (Planet p : Planet.values())\n" +
                 "           System.out.printf(\"Your weight on %s is %f%n\",\n" +
                 "                             p, p.surfaceWeight(mass));\n" +
                 "    }\n" +
                 "}\n" +
                 "If you run Planet.class from the command line with an argument of 175, you get this output:\n" +
                 "\n" +
                 "$ java Planet 175\n" +
                 "Your weight on MERCURY is 66.107583\n" +
                 "Your weight on VENUS is 158.374842\n" +
                 "Your weight on EARTH is 175.000000\n" +
                 "Your weight on MARS is 66.279007\n" +
                 "Your weight on JUPITER is 442.847567\n" +
                 "Your weight on SATURN is 186.552719\n" +
                 "Your weight on URANUS is 158.397260\n" +
                 "Your weight on NEPTUNE is 199.207413", entryEntity, userRepository.findFirstById(2));
        entryVersionRepository.save(entryVersion);

        //------------------------------------------------------------------------------------------------------
        entryEntity = new Entry(userRepository.findFirstById(1));
        entryEntity.setImage(imageRepository.findFirstById(1L));

        entryRepository.save(entryEntity);


        entryEntity.getCategories().clear();
        entryEntity.getCategories().add(categoryRepository.getFirstById(2L));

        entryRepository.save(entryEntity);


         entryVersion = new EntryVersion("MariaDB - Introduction", "A database application exists separate from the main application and stores data collections. Every database employs one or multiple APIs for the creation, access, management, search, and replication of the data it contains.\n" +
                 "\n" +
                 "Databases also use non-relational data sources such as objects or files. However, databases prove the best option for large datasets, which would suffer from slow retrieval and writing with other data sources.\n" +
                 "\n" +
                 "Relational database management systems, or RDBMS, store data in various tables.Relationships between these tables are established using primary keys and foreign keys.\n" +
                 "\n" +
                 "RDBMS offers the following features −\n" +
                 "\n" +
                 "They enable you to implement a data source with tables, columns, and indices.\n" +
                 "\n" +
                 "They ensure the integrity of references across rows of multiple tables.\n" +
                 "\n" +
                 "They automatically update indices.\n" +
                 "\n" +
                 "They interpret SQL queries and operations in manipulating or sourcing data from tables.\n" +
                 "\n" +
                 "RDBMS Terminology\n" +
                 "Before we begin our discussion of MariaDB, let us review a few terms related to databases.\n" +
                 "\n" +
                 "Database − A database is a data source consisting of tables holding related data.\n" +
                 "\n" +
                 "Table − A table, meaning a spreadsheet, is a matrix containing data.\n" +
                 "\n" +
                 "Column − A column, meaning data element, is a structure holding data of one type; for example, shipping dates.\n" +
                 "\n" +
                 "Row − A row is a structure grouping related data; for example, data for a customer. It is also known as a tuple, entry, or record.\n" +
                 "\n" +
                 "Redundancy − This term refers to storing data twice in order to accelerate the system.\n" +
                 "\n" +
                 "Primary Key − This refers to a unique, identifying value. This value cannot appear twice within a table, and there is only one row associated with it.\n" +
                 "\n" +
                 "Foreign Key − A foreign key serves as a link between two tables.\n" +
                 "\n" +
                 "Compound Key − A compound key, or composite key, is a key that refers to multiple columns. It refers to multiple columns due to a column lacking a unique quality.\n" +
                 "\n" +
                 "Index − An index is virtually identical to the index of a book.\n" +
                 "\n" +
                 "Referential Integrity − This term refers to ensuring all foreign key values point to existing rows.\n" +
                 "\n" +
                 "MariaDB Database\n" +
                 "MariaDB is a popular fork of MySQL created by MySQL's original developers. It grew out of concerns related to MySQL's acquisition by Oracle. It offers support for both small data processing tasks and enterprise needs. It aims to be a drop-in replacement for MySQL requiring only a simple uninstall of MySQL and an install of MariaDB. MariaDB offers the same features of MySQL and much more.\n" +
                 "\n" +
                 "Key Features of MariaDB\n" +
                 "The important features of MariaDB are −\n" +
                 "\n" +
                 "All of MariaDB is under GPL, LGPL, or BSD.\n" +
                 "\n" +
                 "MariaDB includes a wide selection of storage engines, including high-performance storage engines, for working with other RDBMS data sources.\n" +
                 "\n" +
                 "MariaDB uses a standard and popular querying language.\n" +
                 "\n" +
                 "MariaDB runs on a number of operating systems and supports a wide variety of programming languages.\n" +
                 "\n" +
                 "MariaDB offers support for PHP, one of the most popular web development languages.\n" +
                 "\n" +
                 "MariaDB offers Galera cluster technology.\n" +
                 "\n" +
                 "MariaDB also offers many operations and commands unavailable in MySQL, and eliminates/replaces features impacting performance negatively.\n" +
                 "\n" +
                 "Getting Started\n" +
                 "Before you begin this tutorial, make sure you have some basic knowledge of PHP and HTML, specifically material discussed in our PHP and HTML tutorials.\n" +
                 "\n" +
                 "This guide focuses on use of MariaDB in a PHP environment, so our examples will be most useful for PHP developers.\n" +
                 "\n" +
                 "We strongly recommend reviewing our PHP Tutorial if you lack familiarity or need to review.", entryEntity, userRepository.findFirstById(1));
        entryVersionRepository.save(entryVersion);

        //------------------------------------------------------------------------------------------------------

         entryEntity = new Entry(userRepository.findFirstById(1));
        entryEntity.setImage(imageRepository.findFirstById(1L));

        entryRepository.save(entryEntity);


        entryEntity.getCategories().clear();
        entryEntity.getCategories().add(categoryRepository.getFirstById(7L));

        entryRepository.save(entryEntity);

        User fill2 = new User("Bernd", "12345");
        userRepository.save(fill2);

        Comment commentEntity = new Comment("Find ich Toll.", fill2, entryEntity);
        commentRepository.save(commentEntity);


         entryVersion = new EntryVersion("Aok Systems kommt zum Terrific Thursday",  "AOK Systems\n" +
                 "Ansprechpartner: Tobias Duerr (Abteilungsleiter Kompetenz Center/CRM/Internet), Matthias Lovrencic (Teamleiter CRM, Entwicklung), Yono K. Philipp Stöhr (Leiter Personal und Organisationsentwicklung)\n" +
                 "Informiert euch gerne unter: http://www.aok-systems.de/ (Links to an external site.)Links to an external site.\n" +
                 "Rekrutieren: Softwareentwickler",entryEntity, userRepository.findFirstById(1));
        entryVersionRepository.save(entryVersion);

        //------------------------------------------------------------------------------------------------------
        entryEntity = new Entry(userRepository.findFirstById(1));
        entryEntity.setImage(imageRepository.findFirstById(1L));

        entryRepository.save(entryEntity);


        entryEntity.getCategories().clear();
        entryEntity.getCategories().add(categoryRepository.getFirstById(4L));

        entryEntity.getCategories().add(categoryRepository.getFirstById(5L));
        entryRepository.save(entryEntity);


        entryVersion = new EntryVersion("HTML vs. CSS vs. Javascript",  "HTML vs. CSS vs. Javascript\n" +
                "HTML, CSS und Javascript sind drei Webstandards, die jeweils einen anderen Aspekt der Webseite abdecken. Deshalb kann man in diesem Zusammenhang auch von einem »Drei-Schichten-Modell« sprechen.\n" +
                "\n" +
                "Das Zusammenwirken von HTML, CSS und JavaScript\n" +
                "Allgemein kann die Verarbeitung eines im Web typischen Dokuments in einem Drei-Schichten-Modell veranschaulicht werden. Die drei Schichten wären die Markup-Schicht mit den HTML-Auszeichnungen, die Präsentationsschicht mit den CSS-Regeln und die Verhaltensschicht mit Javascript. Jede Schicht für sich ist für einen speziellen Teil des Gesamtdokumentes zuständig. Im Idealfall sind alle Schichten voneinander getrennt.\n" +
                "\n" +
                "Die Markup-Schicht\n" +
                "Das so genannte Markup ist das Herzstück einer Webseite und spiegelt die Struktur des Seiteninhaltes wider.\n" +
                "\n" +
                "Handelt es sich bei einem Datensegment um eine Überschrift oder um einen normalen Absatz?\n" +
                "Wohin führt ein Verweis und in welcher Beziehung steht die Ziel-Ressource mit der aktuellen?\n" +
                "In welcher logischen Verbindung stehen die Daten einer Tabelle zueinander?\n" +
                "All diese Fragen beantwortet und beschreibt die Auszeichnungssprache mit ihrem Markup durch so genannte Elemente. Die Elemente spiegeln die Semantik einer Webseite wider (näheres zu diesem Thema folgt in diesem Adventskalender). Für die Darstellung im Webbrowser ist die Auszeichnungssprache entweder HTML oder XHTML.\n" +
                "\n" +
                "Die Präsentationsschicht\n" +
                "In der Präsentationsschicht werden die strukturierten Daten mit Layoutanweisungen kombiniert. So entsteht ein Layout, die Webseite bekommt ein Gesicht. Leider wird noch viel zu oft versucht, ein Layout mit HTML-Mitteln zu erreichen. Das widerspricht der Grundidee von HTML, denn das Markup besitzt für sich keinerlei darstellerische Gestalt. Erst durch die Regeln eines CSS-Stylesheets bekommt es ein Gesicht. Browsern liegen auch CSS-Dateien bei (sogenannte User-Agent-Stylesheets), weshalb jeder von uns eine »ungestylte« Webseite trotzdem dank des Einflusses von CSS-Regeln betrachtet. Schreibt man CSS-Dateien selber, kann man das dem Browser mitgegebene Aussehen von HTML-Elementen fast nach Belieben verändern.\n" +
                "\n" +
                "Die Verhaltensschicht\n" +
                "In der Verhaltensschicht sorgt das so genannte DOM-Scripting für die Dynamik einer Webseite. So können beispielsweise Teile per Tastendruck ein- oder ausgeblendet, ausgewählt, neu angeordnet werden. Die Verhaltensschicht sorgt für die direkte Interaktion einer Webseite mit dem Benutzer.\n" +
                "\n" +
                "Meist ist die Skriptsprache zur Veränderung des DOM JavaScript – nicht zu verwechseln mit der Programmiersprache Java.\n" +
                "\n" +
                "Das Document-Object-Modell\n" +
                "Das DOMdient als gemeinsame Schnittstelle zwischen den einzelnen Schichten. Es erlaubt Programmen und Skripten den dynamischen Zugriff und die Änderung des Inhalts, der Struktur und des Stils eines Dokuments.\n" +
                "\n" +
                "Die Verarbeitung des Dokuments vom Browser\n" +
                "Als erstes durchläuft das Markup den browsereigenen Parser. Nachdem das Markup durch den Parser lief, entwirft der Browser nach einiger Fehlerbehebung das DOM, das als Basis weiterer Verarbeitungsschritte dient. Danach werden die Stylesheet-Daten verarbeitet und den Elemeten des DOMs zugewiesen. Erst dadurch erhalten die Elemente eine Gestalt und können dargestellt werden. Zu guter Letzt werden die Daten der Verhaltensschicht verarbeitet und interpretiert.",entryEntity, userRepository.findFirstById(1));
        entryVersionRepository.save(entryVersion);

        //------------------------------------------------------------------------------------------------------

        entryEntity = new Entry(userRepository.findFirstById(1));
        entryEntity.setImage(imageRepository.findFirstById(1L));

        entryRepository.save(entryEntity);


        entryEntity.getCategories().clear();
        entryEntity.getCategories().add(categoryRepository.getFirstById(3L));


        entryRepository.save(entryEntity);


        User fill = new User("TimTheTroll", "12345");
        userRepository.save(fill);

        Comment commentEntity2 = new Comment("Interessiert mich nicht. Das ist doch alles mist", fill, entryEntity);
        commentRepository.save(commentEntity2);


        entryVersion = new EntryVersion("Spring Boot - Introduction",  "Spring Boot is an open source Java-based framework used to create a micro Service. It is developed by Pivotal Team and is used to build stand-alone and production ready spring applications. This chapter will give you an introduction to Spring Boot and familiarizes you with its basic concepts.\n" +
                "\n" +
                "What is Micro Service?\n" +
                "Micro Service is an architecture that allows the developers to develop and deploy services independently. Each service running has its own process and this achieves the lightweight model to support business applications.\n" +
                "\n" +
                "Advantages\n" +
                "Micro services offers the following advantages to its developers −\n" +
                "\n" +
                "Easy deployment\n" +
                "Simple scalability\n" +
                "Compatible with Containers\n" +
                "Minimum configuration\n" +
                "Lesser production time\n" +
                "What is Spring Boot?\n" +
                "Spring Boot provides a good platform for Java developers to develop a stand-alone and production-grade spring application that you can just run. You can get started with minimum configurations without the need for an entire Spring configuration setup.\n" +
                "\n" +
                "Advantages\n" +
                "Spring Boot offers the following advantages to its developers −\n" +
                "\n" +
                "Easy to understand and develop spring applications\n" +
                "Increases productivity\n" +
                "Reduces the development time\n" +
                "Goals\n" +
                "Spring Boot is designed with the following goals −\n" +
                "\n" +
                "To avoid complex XML configuration in Spring\n" +
                "To develop a production ready Spring applications in an easier way\n" +
                "To reduce the development time and run the application independently\n" +
                "Offer an easier way of getting started with the application\n" +
                "Why Spring Boot?\n" +
                "You can choose Spring Boot because of the features and benefits it offers as given here −\n" +
                "\n" +
                "It provides a flexible way to configure Java Beans, XML configurations, and Database Transactions.\n" +
                "\n" +
                "It provides a powerful batch processing and manages REST endpoints.\n" +
                "\n" +
                "In Spring Boot, everything is auto configured; no manual configurations are needed.\n" +
                "\n" +
                "It offers annotation-based spring application\n" +
                "\n" +
                "Eases dependency management\n" +
                "\n" +
                "It includes Embedded Servlet Container\n" +
                "\n" +
                "How does it work?\n" +
                "Spring Boot automatically configures your application based on the dependencies you have added to the project by using @EnableAutoConfiguration annotation. For example, if MySQL database is on your classpath, but you have not configured any database connection, then Spring Boot auto-configures an in-memory database.\n" +
                "\n" +
                "The entry point of the spring boot application is the class contains @SpringBootApplication annotation and the main method.\n" +
                "\n" +
                "Spring Boot automatically scans all the components included in the project by using @ComponentScan annotation.\n" +
                "\n" +
                "Spring Boot Starters\n" +
                "Handling dependency management is a difficult task for big projects. Spring Boot resolves this problem by providing a set of dependencies for developers convenience.\n" +
                "\n" +
                "For example, if you want to use Spring and JPA for database access, it is sufficient if you include spring-boot-starter-data-jpa dependency in your project.\n" +
                "\n" +
                "Note that all Spring Boot starters follow the same naming pattern spring-boot-starter- *, where * indicates that it is a type of the application.\n" +
                "\n" +
                "Examples\n" +
                "Look at the following Spring Boot starters explained below for a better understanding −\n" +
                "\n" +
                "Spring Boot Starter Actuator dependency is used to monitor and manage your application. Its code is shown below −\n" +
                "\n" +
                "<dependency>\n" +
                "   <groupId>org.springframework.boot</groupId>\n" +
                "   <artifactId>spring-boot-starter-actuator</artifactId>\n" +
                "</dependency>\n" +
                "Spring Boot Starter Security dependency is used for Spring Security. Its code is shown below −\n" +
                "\n" +
                "<dependency>\n" +
                "   <groupId>org.springframework.boot</groupId>\n" +
                "   <artifactId>spring-boot-starter-security</artifactId>\n" +
                "</dependency>\n" +
                "Spring Boot Starter web dependency is used to write a Rest Endpoints. Its code is shown below −\n" +
                "\n" +
                "<dependency>\n" +
                "   <groupId>org.springframework.boot</groupId>\n" +
                "   <artifactId>spring-boot-starter-web</artifactId>\n" +
                "</dependency>\n" +
                "Spring Boot Starter Thyme Leaf dependency is used to create a web application. Its code is shown below −\n" +
                "\n" +
                "<dependency>\n" +
                "   <groupId>org.springframework.boot</groupId>\n" +
                "   <artifactId>spring-boot-starter-thymeleaf</artifactId>\n" +
                "</dependency>\n" +
                "Spring Boot Starter Test dependency is used for writing Test cases. Its code is shown below −\n" +
                "\n" +
                "<dependency>\n" +
                "   <groupId>org.springframework.boot</groupId>\n" +
                "   <artifactId>spring-boot-starter-test<artifactId>\n" +
                "</dependency>\n" +
                "Auto Configuration\n" +
                "Spring Boot Auto Configuration automatically configures your Spring application based on the JAR dependencies you added in the project. For example, if MySQL database is on your class path, but you have not configured any database connection, then Spring Boot auto configures an in-memory database.\n" +
                "\n" +
                "For this purpose, you need to add @EnableAutoConfiguration annotation or @SpringBootApplication annotation to your main class file. Then, your Spring Boot application will be automatically configured.\n" +
                "\n" +
                "Observe the following code for a better understanding −\n" +
                "\n" +
                "import org.springframework.boot.SpringApplication;\n" +
                "import org.springframework.boot.autoconfigure.EnableAutoConfiguration;\n" +
                "\n" +
                "@EnableAutoConfiguration\n" +
                "public class DemoApplication {\n" +
                "   public static void main(String[] args) {\n" +
                "      SpringApplication.run(DemoApplication.class, args);\n" +
                "   }\n" +
                "}\n" +
                "Spring Boot Application\n" +
                "The entry point of the Spring Boot Application is the class contains @SpringBootApplication annotation. This class should have the main method to run the Spring Boot application. @SpringBootApplication annotation includes Auto- Configuration, Component Scan, and Spring Boot Configuration.\n" +
                "\n" +
                "If you added @SpringBootApplication annotation to the class, you do not need to add the @EnableAutoConfiguration, @ComponentScan and @SpringBootConfiguration annotation. The @SpringBootApplication annotation includes all other annotations.\n" +
                "\n" +
                "Observe the following code for a better understanding −\n" +
                "\n" +
                "import org.springframework.boot.SpringApplication;\n" +
                "import org.springframework.boot.autoconfigure.SpringBootApplication;\n" +
                "\n" +
                "@SpringBootApplication\n" +
                "public class DemoApplication {\n" +
                "   public static void main(String[] args) {\n" +
                "      SpringApplication.run(DemoApplication.class, args);\n" +
                "   }\n" +
                "}\n" +
                "Component Scan\n" +
                "Spring Boot application scans all the beans and package declarations when the application initializes. You need to add the @ComponentScan annotation for your class file to scan your components added in your project.\n" +
                "\n" +
                "Observe the following code for a better understanding −\n" +
                "\n" +
                "import org.springframework.boot.SpringApplication;\n" +
                "import org.springframework.context.annotation.ComponentScan;\n" +
                "\n" +
                "@ComponentScan\n" +
                "public class DemoApplication {\n" +
                "   public static void main(String[] args) {\n" +
                "      SpringApplication.run(DemoApplication.class, args);\n" +
                "   }\n" +
                "}", entryEntity,userRepository.findFirstById(1));
        entryVersionRepository.save(entryVersion);

        //------------------------------------------------------------------------------------------------------








    }






    private void addPics() throws IOException {
        List<File> filesInFolder = Files.walk(Paths.get(".\\src\\main\\resources\\Images"))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());

        for(File f: filesInFolder) {
            byte[] arrayPic = Files.readAllBytes(f.toPath());


            Image newImage = new Image(f.getName(), "JPG", arrayPic);
            imageRepository.save(newImage);

        }



    }

    private void makeCategorys() {
        Category categoryJ = new Category("Java");
        categoryRepository.save(categoryJ);

        Category  categoryM = new Category("MariaDB");
        categoryRepository.save(categoryM);

        Category categoryS = new Category("SpringBoot");
        categoryRepository.save(categoryS);

        Category  categoryJS = new Category("JavaScript");
        categoryRepository.save(categoryJS);

        Category  categoryH = new Category("Html/Css");
        categoryRepository.save(categoryH);
        Category categoryA = new Category("Angular");
        categoryRepository.save(categoryA);


        Category  categoryTT = new Category("TT");
        categoryRepository.save(categoryTT);
    }

    private void makeUsers() {
        User fill = new User("Yun", "12345");
        userRepository.save(fill);

        fill = new User("Andreas", "12345");
        userRepository.save(fill);
        fill = new User("Anika", "12345");
        userRepository.save(fill);
        fill = new User("Ben", "12345");
        userRepository.save(fill);
        fill = new User("Benny", "12345");
        userRepository.save(fill);
        fill = new User("Carina", "12345");
        userRepository.save(fill);
        fill = new User("Cornelia", "12345");
        userRepository.save(fill);
        fill = new User("Cristina", "12345");
        userRepository.save(fill);
        fill = new User("Denise", "12345");
        userRepository.save(fill);
        fill = new User("Derya", "12345");
        userRepository.save(fill);
        fill = new User("Diana", "12345");
        userRepository.save(fill);
        fill = new User("Dorothee", "12345");
        userRepository.save(fill);
        fill = new User("Hürsad", "12345");
        userRepository.save(fill);
        fill = new User("Jesse", "12345");
        userRepository.save(fill);
        fill = new User("Johannes", "12345");
        userRepository.save(fill);
        fill = new User("Jose", "12345");
        userRepository.save(fill);
        fill = new User("Leif", "12345");
        userRepository.save(fill);
        fill = new User("Leonardo", "12345");
        userRepository.save(fill);
        fill = new User("Marko", "12345");
        userRepository.save(fill);
        fill = new User("MarkusG", "12345");
        userRepository.save(fill);
        fill = new User("Mathias", "12345");
        userRepository.save(fill);
        fill = new User("Michael", "12345");
        userRepository.save(fill);
        fill = new User("Sandra", "12345");
        userRepository.save(fill);
        fill = new User("Rowaa", "12345");
        userRepository.save(fill);
        fill = new User("Simon", "12345");
        userRepository.save(fill);
        fill = new User("Til", "12345");
        userRepository.save(fill);
        fill = new User("Timo", "12345");
        userRepository.save(fill);
    }



}



