package de.awacademy.test.webblog.images;

import de.awacademy.test.webblog.entryVersion.EntryVersion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ImageRepository extends JpaRepository<Image, Long> {

Image findFirstById(long id);
List<Image> findAllByOrderById();
void deleteImageById(long id );
Image findFirstByName(String name);
}
