package de.awacademy.test.webblog.images;


import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ImageService {
    @Autowired
    ImageRepository imageRepository;

    @Transactional
    public String getImage(Long id) {

        byte[] bytes = imageRepository.findFirstById(id).getPic();
        return Base64.encodeBase64String(bytes);


    }

}
