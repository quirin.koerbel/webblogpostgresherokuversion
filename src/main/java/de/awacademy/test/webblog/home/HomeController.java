package de.awacademy.test.webblog.home;

import de.awacademy.test.webblog.category.CategoryRepository;
import de.awacademy.test.webblog.entry.EntryRepository;
import de.awacademy.test.webblog.login.LoginDTO;
import de.awacademy.test.webblog.session.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @Autowired
    private EntryRepository entryRepository;

    @Autowired
    CategoryRepository categoryRepository;


    @GetMapping("/")
    @Transactional
    public String home(Model model) {

        model.addAttribute("categorys", categoryRepository.getAllByOrderByName());
        model.addAttribute("entries", entryRepository.findAllByOrderByCreationDateDesc());
        model.addAttribute("login", new LoginDTO());
        return "home";
    }

    @GetMapping("/category")
    @Transactional
    public String byCategory(Model model, @RequestParam("id") long id) {

        model.addAttribute("categorys", categoryRepository.getAllByOrderByName());
        model.addAttribute("entries", entryRepository.findAllByCategoriesContainsOrderByCreationDateDesc(
                categoryRepository.getFirstById(id)));
        model.addAttribute("login", new LoginDTO());
        return "home";
    }

}
