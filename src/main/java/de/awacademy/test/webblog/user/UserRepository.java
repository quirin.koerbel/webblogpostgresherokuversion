package de.awacademy.test.webblog.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findFirstByNameAndPassword(String name, String password);
    User findFirstById(long id);

    List<User> findAllByAdministratorEqualsOrderByName(boolean isAdministrator);

    List<User> findAllByAdministratorEqualsAndNameIgnoreCaseContainingOrderByName(boolean isAdministrator, String userName);
    User findFirstByName(String name);



    boolean existsByName(String name);

}
