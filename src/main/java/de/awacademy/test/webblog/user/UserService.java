package de.awacademy.test.webblog.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    /**
     * checks the user for administrator rights
     *
     * @param user
     */
    public static boolean checkUserforAdmin(User user){
        if (user == null){
            return false;
        }
        return user.isAdministrator();
    }

    public void makeAdmin(long userId){
        User newAdmin = userRepository.findFirstById(userId);
        newAdmin.setAdministrator(true);
        userRepository.save(newAdmin);
    }

}
