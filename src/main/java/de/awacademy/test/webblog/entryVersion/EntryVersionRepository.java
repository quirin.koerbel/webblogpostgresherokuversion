package de.awacademy.test.webblog.entryVersion;

import de.awacademy.test.webblog.category.Category;
import de.awacademy.test.webblog.entry.Entry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EntryVersionRepository extends JpaRepository<EntryVersion, Long> {

    void deleteByEntryId(long entryId);

}
