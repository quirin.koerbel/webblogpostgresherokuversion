package de.awacademy.test.webblog.category;

import de.awacademy.test.webblog.entry.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.awacademy.test.webblog.comment.CommentDTO;
import de.awacademy.test.webblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CategoryController {

    @Autowired
    CategoryService categoryService;
    @Autowired
    CategoryRepository categoryRepository;



    @PostMapping("/categoryadmin")
    public String categoryView(Model model) {

        model.addAttribute("category", new CategoryDTO());
        model.addAttribute("categorys", categoryRepository.getAllByOrderByName());
        return "cadmin";
    }

    @PostMapping("/saveCategory")
    public String createCategory(Model model,  @ModelAttribute("category") CategoryDTO category,
                         BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser) {

        categoryService.saveNewCategory(currentUser,category );


        model.addAttribute("category", new CategoryDTO());

        model.addAttribute("categorys", categoryRepository.getAllByOrderByName() );
        return "cadmin";
    }

    @GetMapping("/categoryadmin")
    public String categoryViewGet(Model model) {

        model.addAttribute("category", new CategoryDTO());
        model.addAttribute("categorys", categoryRepository.getAllByOrderByName());
        return "cadmin";
    }



    @PostMapping("/deleteCategory")
    public String deleteCategory(Model model, @RequestParam("categoryId") long categoryId, @ModelAttribute("category") CategoryDTO category,
                                 BindingResult bindingResult, @ModelAttribute("currentUser") User currentUser) {


        categoryService.deleteCategory(currentUser,categoryId );


        model.addAttribute("category", new CategoryDTO());

        model.addAttribute("categorys", categoryRepository.getAllByOrderByName() );
        return "cadmin";
    }



}
