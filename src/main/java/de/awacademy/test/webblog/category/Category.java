package de.awacademy.test.webblog.category;

import de.awacademy.test.webblog.entry.Entry;

import javax.persistence.*;
import java.util.List;


@Entity
public class Category {




    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;


    @ManyToMany (mappedBy = "categories")
    private List<Entry> entrys;

    public void setId(long id) {
        this.id = id;
    }

    public List<Entry> getEntrys() {
        return entrys;
    }

    public void setEntrys(List<Entry> entrys) {
        this.entrys = entrys;
    }

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
