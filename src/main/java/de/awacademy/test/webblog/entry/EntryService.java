package de.awacademy.test.webblog.entry;

import de.awacademy.test.webblog.category.Category;
import de.awacademy.test.webblog.category.CategoryRepository;
import de.awacademy.test.webblog.comment.CommentRepository;
import de.awacademy.test.webblog.entryVersion.EntryVersion;
import de.awacademy.test.webblog.entryVersion.EntryVersionRepository;
import de.awacademy.test.webblog.images.Image;
import de.awacademy.test.webblog.images.ImageRepository;
import de.awacademy.test.webblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class EntryService {

    @Autowired
    EntryRepository entryRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    EntryVersionRepository entryVersionRepository;
    @Autowired
    ImageRepository imageRepository;


    public Entry createNewEntry(Model model, EntryDTO entry, User user) throws IOException {
        Entry entryEntity = new Entry(user);

        entryEntity.getCategories().clear();
        for (Long i : entry.getCategorys()) {
            entryEntity.getCategories().add(categoryRepository.getFirstById(i));

        }


        byte[] arrayPic = entry.getFile().getBytes();
        if (arrayPic.length > 1) {

            Image newImage = new Image(entry.getTitle(), entry.getFile().getContentType(), arrayPic);
            imageRepository.save(newImage);
            entryEntity.setImage(newImage);

        } else {

            entryEntity.setImage(imageRepository.findFirstById(1));

        }

        entryRepository.save(entryEntity);
        model.addAttribute("entryToEdit", entryEntity);


        EntryVersion entryVersion = new EntryVersion(entry.getTitle(), entry.getText(), entryEntity, user);
        entryVersionRepository.save(entryVersion);

        return entryEntity;
    }


    public Entry saveEditedEntry(Model model, long entryToEditId, EntryDTO entry, User user) throws IOException {

        Entry entryToEdit = entryRepository.findFirstById(entryToEditId);
        EntryVersion entryVersion = new EntryVersion(entry.getTitle(), entry.getText(), entryToEdit, user);


        byte[] arrayPic = entry.getFile().getBytes();
        if (arrayPic.length > 1) {
            Image newImage = new Image(entry.getTitle(), entry.getFile().getContentType(), arrayPic);
            imageRepository.save(newImage);

            Image toDel = entryToEdit.getImage();
            entryToEdit.setImage(newImage);
            imageRepository.delete(toDel);

        }

        entryVersionRepository.save(entryVersion);

        entryToEdit.getCategories().clear();
        for (Long i : entry.getCategorys()) {
            entryToEdit.getCategories().add(categoryRepository.getFirstById(i));

        }

        entryRepository.save(entryToEdit);

        model.addAttribute("entryToEdit", entryToEdit);

        return entryToEdit;
    }


    public void addAttributesToModelForEditingEntry(Model model, long entryToEditId) {

        EntryDTO ourEntryDTO = new EntryDTO();
        Entry entry = entryRepository.findFirstById(entryToEditId);

        List<Long> ids = new LinkedList<>();
        for (Category category : entry.getCategories()) {
            ids.add(category.getId());
        }

        ourEntryDTO.setCategorys(ids);

        ourEntryDTO.setTitle(entry.getLatestTitle());
        ourEntryDTO.setText(entry.getLatestText());

        model.addAttribute("entry", ourEntryDTO);
        model.addAttribute("entryToEdit", entry);
    }


    public void deleteEntryWithComments(Model model, long entryId) {

        long imageId = entryRepository.findFirstById(entryId).getImage().getId();
        if (imageId != 1) {
            imageRepository.deleteImageById(imageId);


        }
        commentRepository.deleteByEntry(entryRepository.findFirstById(entryId));
        entryVersionRepository.deleteByEntryId(entryId);
        entryRepository.findFirstById(entryId).getCategories().clear();
        entryRepository.save(entryRepository.findFirstById(entryId));

        entryRepository.deleteById(entryId);

    }


}