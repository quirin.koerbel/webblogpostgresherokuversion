package de.awacademy.test.webblog.entry;

import de.awacademy.test.webblog.category.Category;
import de.awacademy.test.webblog.entryVersion.EntryVersion;
import de.awacademy.test.webblog.images.Image;
import de.awacademy.test.webblog.user.User;
import de.awacademy.test.webblog.user.UserRepository;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Entry {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;


    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "entry_category",
            joinColumns = {@JoinColumn(name = "entry_id")},
            inverseJoinColumns = {@JoinColumn(name = "category_id")}
    )
    private List<Category> categories;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "entry")
    @OrderBy("changeDate desc")
    private List<EntryVersion> entryVersions;

    @ManyToOne
    private User user;

    private Instant creationDate;


    @ManyToOne
    private Image image;


    public String getImageAsString() {
        return Base64.encodeBase64String(image.getPic());
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Entry() {
    }


    public long getId() {
        return id;
    }

    public Entry(User user) {
        this.user = user;
        this.creationDate = Instant.now();
        this.categories = new ArrayList<>();
    }

    public String getTitleByVersionId(long versionId) {
        int index = 0;
        while (entryVersions.get(index).getId() != versionId) {
            index++;
        }
        return entryVersions.get(index).getTitle();
    }

    public String getTextByVersionId(long versionId) {
        int index = 0;
        while (entryVersions.get(index).getId() != versionId) {
            index++;
        }
        return entryVersions.get(index).getText();
    }

    public User getUserByVersionId(long versionId) {
        int index = 0;
        while (entryVersions.get(index).getId() != versionId) {
            index++;
        }
        return entryVersions.get(index).getUser();
    }

    public String getLatestTitle() {
        if (entryVersions.size() == 0) {
            return "";
        }
        return entryVersions.get(0).getTitle();
    }

    public String getLatestText() {
        if (entryVersions.size() == 0) {
            return "";
        }
        return entryVersions.get(0).getText();
    }

    public String getLatestTextCut() {
        if (entryVersions.size() == 0) {
            return "";
        }
        if (entryVersions.get(0).getText().length() > 200) {
            return entryVersions.get(0).getText().substring(0, 200) + "...";
        }
        return entryVersions.get(0).getText().substring(0, entryVersions.get(0).getText().length());
    }

    public Instant getLatestChangeDate() {
        if (entryVersions.size() == 0) {
            return null;
        }
        return entryVersions.get(0).getChangeDate();
    }

    public User getLatestChanger() {
        if (entryVersions.size() == 0) {
            return entryVersions.get(0).getUser();
        }
        return entryVersions.get(0).getUser();
    }

    public User getUser() {
        return user;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public List<EntryVersion> getEntryVersions() {
        return entryVersions;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }


}
