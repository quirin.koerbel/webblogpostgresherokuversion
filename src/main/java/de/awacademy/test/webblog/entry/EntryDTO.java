package de.awacademy.test.webblog.entry;





import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class EntryDTO {

    @NotEmpty
    private String title = "";

    @NotEmpty
    private String text = "";

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    private List<Long> categorys;

    public List<Long> getCategorys() {
        return categorys;
    }

    public void setCategorys(List<Long> categorys) {
        this.categorys = categorys;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
