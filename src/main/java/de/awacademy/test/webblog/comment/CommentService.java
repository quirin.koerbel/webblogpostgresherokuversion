package de.awacademy.test.webblog.comment;

import de.awacademy.test.webblog.entry.EntryRepository;
import de.awacademy.test.webblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
public class CommentService {

    @Autowired
    EntryRepository entryRepository;
    @Autowired
    CommentRepository commentRepository;

    public void refreshModelAttributesWithEntryAndComments(Model model, long entryId) {

        model.addAttribute("comment", new CommentDTO());
        model.addAttribute("comments", commentRepository.findAllByEntryOrderByCreationDateAsc(entryRepository.findFirstById(entryId)));
        model.addAttribute("entry", entryRepository.findFirstById(entryId));

    }

    public void addNewComment(CommentDTO comment, User user, long entryId){

        if (!(comment.getText().length() < 5 || user == null)) {

            Comment commentEntity = new Comment(comment.getText(), user, entryRepository.findFirstById(entryId));
            commentRepository.save(commentEntity);

        }

    }

    public void deleteComment(long commentId, User user){

        if (user != null) {
            if (user.isAdministrator() || user.getId() == commentRepository.findFirstById(commentId).getUser().getId()) {
                commentRepository.deleteById(commentId);
            }
        }

    }

}
